# This is an unsupported codebase.  It should be functional, but isn't supported.

# Pi File Upload API

## Uploading
POST zipfiles to /rest/upload. The file's key value should be 'data_file'.

A successful upload will have the HTTP status code of 200 and 'success' in the response content.

## Error Messages

### HTTP Status Code 405

GET requests are not permitted.

### HTTP Status Code 415

This indicates a problem with the uploaded zipfile. The response content will be a JSON dict with each key in the dict corresponding to the key in the POST request. Each dict entry will have an array of issues.

Example issues:
'Invalid extension. Must be .zip.'

'Corrupted zipfile.'

# Sphinx Documentation
To re-generate sphix source files:

```sphinx-apidoc -f -e -o docs\source .```

To re-generate html:

```docs\make html```

# Sensor alpha values

## Aeth
AE51-S6-970: alpha = 0.9318

AE51-S6-971: alpha = 1.0

AE51-S6-974: alpha = 1.0005

AE51-S6-975: alpha = 0.9509

## CPC
3007-02150002: alpha = 1.0

3007-02150003: alpha = 1.017501

3783-151902:   alpha = 1.0

# Sensor beta values

## Aeth
AE51-S6-970: beta = unknown (sensor not used for measurements)

AE51-S6-971: beta = -4.522785313

AE51-S6-974: beta = 15.72594747

AE51-S6-975: beta = 9.923162074

## CPC
3007-02150002: beta = 1.0833

3007-02150003: beta = 1.0833

3783-151902:   beta = 1.0


# Helpful Scripts

## SQL Table of run id, count of spurious observations, and total measurements

```
SELECT total_counts.run_id, so_counts.so_measurements, total_counts.total_measurements FROM
(SELECT rest_run.id AS run_id, COUNT(*) AS total_measurements FROM rest_aeth JOIN rest_measurement ON rest_aeth.measurement_ptr_id = rest_measurement.id JOIN rest_recording ON rest_measurement.recording_id = rest_recording.id JOIN rest_run on rest_recording.run_id = rest_run.id GROUP BY rest_run.id ORDER BY rest_run.id) AS total_counts
INNER JOIN
(SELECT rest_run.id AS run_id, COUNT(*) AS so_measurements FROM rest_aeth JOIN rest_measurement ON rest_aeth.measurement_ptr_id = rest_measurement.id JOIN rest_recording ON rest_measurement.recording_id = rest_recording.id JOIN rest_run on rest_recording.run_id = rest_run.id WHERE so = true GROUP BY rest_run.id ORDER BY rest_run.id) AS so_counts
ON so_counts.run_id = total_counts.run_id;
```

## Django shell Re-run just the first pi upload

```
from rest.models import pi_uploads, run
from rest.importer.tool import DataImporter
run.objects.all().delete()
upload = pi_uploads.objects.first()
i = DataImporter(upload)
i.open_sensor_log()
errors = i.import_now()
i.release()

upload.processed=True
if errors:
	upload.processing_errors=errors
upload.save()
```

## Django shell re-run all data

```
from rest.models import pi_uploads, run
from rest.importer.tool import DataImporter
run.objects.all().delete()
for upload in pi_uploads.objects.all():
	i = DataImporter(upload)
	i.open_sensor_log()
	errors = i.import_now()
	i.release()
	
	upload.processed=True
	if errors:
		upload.processing_errors=errors
	upload.save()
```