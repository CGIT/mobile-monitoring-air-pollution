from django.conf.urls import url
from django.contrib.gis import admin
from django.core.urlresolvers import reverse
from django.http.response import HttpResponseRedirect

from rest.importer.tool import DataImporter
from rest.models import path, run, recording, sensor, measurement, cpc, aeth, gps, pi_uploads


# Register your models here.
class Pi_UploadsAdmin(admin.OSMGeoAdmin):
    readonly_fields=('data_file', 'date_uploaded', 
#                      'processed', 
                     'processing_errors')
    
    def get_urls(self):
        urls = super(Pi_UploadsAdmin, self).get_urls()
        my_urls = [
                    url(r'^(?P<pi_upload_id>[0-9]+)/process/$', self.admin_site.admin_view(self.process), name='rest_pi_uploads_process'),
        ]
        return my_urls + urls
    
    def process(self, request, pi_upload_id):
        upload = pi_uploads.objects.get(id=pi_upload_id)
        
        if not upload.processed:
            i = DataImporter(upload)
            i.open_sensor_log()
            errors = i.import_now()
            i.release()
            
            upload.processed=True
            if errors:
                upload.processing_errors=errors
            upload.save()
        
        return HttpResponseRedirect(reverse("admin:rest_pi_uploads_change", args=[pi_upload_id]))

admin.site.register(path, admin.OSMGeoAdmin)
admin.site.register(run, admin.OSMGeoAdmin)
admin.site.register(recording, admin.OSMGeoAdmin)
admin.site.register(sensor, admin.OSMGeoAdmin)
admin.site.register(measurement, admin.OSMGeoAdmin)
admin.site.register(cpc, admin.OSMGeoAdmin)
admin.site.register(aeth, admin.OSMGeoAdmin)
admin.site.register(gps, admin.OSMGeoAdmin)
admin.site.register(pi_uploads, Pi_UploadsAdmin)