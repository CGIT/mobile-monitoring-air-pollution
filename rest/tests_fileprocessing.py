import csv
import os, json, tempfile
from tempfile import NamedTemporaryFile, _TemporaryFileWrapper
from unittest.case import skip
import zipfile

from django.contrib.gis.geos.linestring import LineString
from django.core.urlresolvers import reverse
from django.test import TestCase, Client

from rest.importer.tool import DataImporter
from rest.models import *


# Create your tests here.
class FilePreparation(TestCase):
    def setUp(self):
        self.files = os.path.join(os.path.dirname(__file__), 'test_resources/FilePreparation')
    
    def test_splitting(self):
        with open(os.path.join(self.files, 'example.zip'), 'rb') as fp:
            c = Client()
            data = {
                "data_file" : fp,
                }
            response = c.post(reverse('upload'), data)
        
        p = pi_uploads.objects.all().last()
        i = DataImporter(p)
        i.open_sensor_log()
        
        # Make sure two files were split out
        self.assertTrue(isinstance(i.data_handle, _TemporaryFileWrapper), "Data handle is not a named temporary file")
        self.assertTrue(isinstance(i.runs_handle, _TemporaryFileWrapper), "Runs handle is not a named temporary file")
        
        # Check data handle rows, column, and header
        i.data_handle.seek(0)
        datareader = csv.reader(i.data_handle)
        dataheader = datareader.next()
        datarowcount = 1 # start at 1 because of reading header 
        datamaxcol = 0
        for row in datareader:
            datarowcount += 1
            if len(row) > datamaxcol:
                datamaxcol = len(row)
                 
        self.assertEqual(','.join(dataheader), "date,run_uid,ath,cpc,gps", "Data file headers incorrect. Given: {}".format(','.join(dataheader)))
        self.assertEqual(datamaxcol, 5, "Incorrect number of columns. Given: {}".format(datamaxcol))
        self.assertEqual(datarowcount, 83, "Incorrect number of rows. Given: {}".format(datarowcount))
        
        
        # Check runs handle rows, column, and header
        i.runs_handle.seek(0)
        runsreader = csv.reader(i.runs_handle)
        runsheader = runsreader.next()
        runsrowcount = 1 # start at 1 because of reading header 
        runsmaxcol = 0
        for row in runsreader:
            runsrowcount += 1
            if len(row) > runsmaxcol:
                runsmaxcol = len(row)
                
        self.assertEqual(','.join(runsheader), "date,run_uid,pi_serial,ath_serial,cpc_serial,gps_serial", "Data file headers incorrect. Given: {}".format(','.join(dataheader)))
        self.assertEqual(runsmaxcol, 6, "Incorrect number of columns. Given: {}".format(runsmaxcol))
        self.assertEqual(runsrowcount, 2, "Incorrect number of rows. Given: {}".format(runsrowcount))
        
        # Make sure releasing the tempfiles happens properly
        i.release()
        
        self.assertTrue(i.data_handle.closed, "Data handle not closed")
        self.assertTrue(i.runs_handle.closed, "Runs handle not closed")


class FileProcessing(TestCase):
    def setUp(self):
        self.files = os.path.join(os.path.dirname(__file__), 'test_resources/FileProcessing')
        
        cpc.objects.all().delete()
        sensor.objects.all().delete()
        aeth.objects.all().delete()
        gps.objects.all().delete()
        recording.objects.all().delete()
        run.objects.all().delete()
        path.objects.all().delete()
        
        self.p = path.objects.create(shape=LineString((0, 0), (0, 50), (50, 50), (50, 0), (0, 0)))
        
    def test_import(self):
        with open(os.path.join(self.files, 'example.zip'), 'rb') as fp:
            c = Client()
            data = {
                "data_file" : fp,
                }
            response = c.post(reverse('upload'), data)
        
        p = pi_uploads.objects.all().last()
        i = DataImporter(p)
        
        with open(os.path.join(self.files, 'example/data.csv'), 'rb') as data, \
        open(os.path.join(self.files, 'example/runs.csv'), 'rb') as runs:
            i.data_handle = data 
            i.runs_handle = runs
            
            errors = i.import_now()
            
            self.assertEqual(len(errors), 5, "Finished with errors. {}".format(errors))

            self.assertEqual(run.objects.count(), 1, "Incorrect number runs. Given {}".format(run.objects.count()))
            self.assertEqual(recording.objects.count(), 82, "Incorrect number recordings. Given {}".format(recording.objects.count()))
            self.assertEqual(sensor.objects.count(), 3, "Incorrect number sensors. Given {}".format(sensor.objects.count()))
            self.assertEqual(measurement.objects.count(), 243, "Incorrect number measurements. Given {}".format(measurement.objects.count()))
            self.assertEqual(cpc.objects.count(), 81, "Incorrect number cpc measurements. Given {}".format(cpc.objects.count()))
            self.assertEqual(aeth.objects.count(), 81, "Incorrect number aeth measurements. Given {}".format(aeth.objects.count()))
            self.assertEqual(gps.objects.count(), 81, "Incorrect number gps measurements. Given {}".format(gps.objects.count()))
            
            i.release()
            self.assertTrue(i.data_handle.closed, "Data handle not closed")
            self.assertTrue(i.runs_handle.closed, "Runs handle not closed")