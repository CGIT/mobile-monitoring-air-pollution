# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rest', '0008_auto_20151005_1303'),
    ]

    operations = [
        migrations.AddField(
            model_name='measurement',
            name='sensor',
            field=models.ForeignKey(default=1, to='rest.sensor'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='sensor',
            name='category',
            field=models.CharField(default='cpc', max_length=5, choices=[(b'cpc', b'CPC'), (b'aeth', b'Aeth')]),
            preserve_default=False,
        ),
    ]
