# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rest', '0010_remove_sensor_django_model_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='path',
            name='name',
            field=models.CharField(default='Downtown', max_length=250),
            preserve_default=False,
        ),
    ]
