# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('rest', '0013_aeth'),
    ]

    operations = [
        migrations.CreateModel(
            name='gps',
            fields=[
                ('measurement_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='rest.measurement')),
                ('point', django.contrib.gis.db.models.fields.PointField(srid=4326)),
            ],
            options={
            },
            bases=('rest.measurement',),
        ),
        migrations.AlterField(
            model_name='sensor',
            name='category',
            field=models.CharField(max_length=5, choices=[(b'cpc', b'CPC'), (b'aeth', b'Aeth'), (b'gps', b'GPS')]),
            preserve_default=True,
        ),
    ]
