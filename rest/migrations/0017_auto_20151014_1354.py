# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('rest', '0016_auto_20151014_1347'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recording',
            name='computer_timestamp',
            field=models.DateTimeField(default=datetime.datetime(2015, 10, 14, 17, 54, 21, 941000, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
