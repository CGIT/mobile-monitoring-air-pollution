# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rest', '0012_auto_20151005_1420'),
    ]

    operations = [
        migrations.CreateModel(
            name='aeth',
            fields=[
                ('measurement_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='rest.measurement')),
                ('bcc', models.IntegerField()),
            ],
            options={
            },
            bases=('rest.measurement',),
        ),
    ]
