# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rest', '0006_remove_measurement_timestamp'),
    ]

    operations = [
        migrations.AddField(
            model_name='measurement',
            name='timestamp',
            field=models.DateTimeField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='recording',
            name='computer_timestamp',
            field=models.DateTimeField(null=True),
            preserve_default=True,
        ),
    ]
