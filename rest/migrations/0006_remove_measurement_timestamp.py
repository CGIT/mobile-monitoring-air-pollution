# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rest', '0005_remove_recording_computer_timestamp'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='measurement',
            name='timestamp',
        ),
    ]
