# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2017-06-27 16:46
from __future__ import unicode_literals

from decimal import Decimal
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rest', '0043_auto_20170620_1525'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sensor',
            name='alpha',
            field=models.DecimalField(decimal_places=9, default=Decimal('1'), help_text=b'FOR CPC USE: Use this field for intra-device corrections. Used as: y = alpha * x ; where y is corrected concentration and x is the uncorrected (3007 03) concentration. FOR AETH USE: Use this field for intra-device corrections. Used as: y = alpha * x; where y is corrected concentration and x is the S6 970 concentration.', max_digits=11),
        ),
        migrations.AlterField(
            model_name='sensor',
            name='beta',
            field=models.DecimalField(decimal_places=9, default=Decimal('1'), help_text=b'FOR CPC USE: Use this field for correcting to base station measurement. Used as: y = alpha * x ; where y is corrected concentration and x is the uncorrected (3007 03) concentration.', max_digits=11),
        ),
    ]
