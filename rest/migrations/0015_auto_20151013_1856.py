# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rest', '0014_auto_20151013_1640'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='aeth',
            name='bcc',
        ),
        migrations.AddField(
            model_name='aeth',
            name='error',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='aeth',
            name='flow',
            field=models.FloatField(default=0.0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='aeth',
            name='ref_val',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='aeth',
            name='sensor_val',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
    ]
