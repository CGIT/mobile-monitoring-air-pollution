# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rest', '0011_path_name'),
    ]

    operations = [
        migrations.RenameField(
            model_name='cpc',
            old_name='count',
            new_name='ppcc',
        ),
    ]
