# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rest', '0004_auto_20150918_1234'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='recording',
            name='computer_timestamp',
        ),
    ]
