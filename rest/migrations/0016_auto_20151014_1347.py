# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rest', '0015_auto_20151013_1856'),
    ]

    operations = [
        migrations.AddField(
            model_name='aeth',
            name='bcc',
            field=models.FloatField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='aeth',
            name='ref_smoothed',
            field=models.FloatField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='aeth',
            name='sensor_smoothed',
            field=models.FloatField(null=True),
            preserve_default=True,
        ),
    ]
