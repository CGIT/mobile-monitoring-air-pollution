# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rest', '0009_auto_20151005_1335'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sensor',
            name='django_model_name',
        ),
    ]
