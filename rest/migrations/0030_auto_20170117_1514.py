# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2017-01-17 20:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rest', '0029_cpc_alpha'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cpc',
            name='alpha',
        ),
        migrations.AddField(
            model_name='sensor',
            name='alpha',
            field=models.DecimalField(decimal_places=9, default=1.0, help_text=b'Use this field for intra-device corrections. Used as: y = alpha * x ; where y is corrected concentration and x is the uncorrected (3007 03) concentration.', max_digits=10),
        ),
    ]
