import csv
import os, json, tempfile
from tempfile import NamedTemporaryFile, _TemporaryFileWrapper
from unittest.case import skip
import zipfile

from django.contrib.auth.models import User
from django.contrib.gis.geos.linestring import LineString
from django.core.files.base import File
from django.core.urlresolvers import reverse
from django.test import TestCase, Client

from rest.importer.tool import DataImporter
from rest.models import *


# Create your tests here.
class Pi_UploadsAdminTest(TestCase):
    def setUp(self):
        self.files = os.path.join(os.path.dirname(__file__), 'test_resources/FilePreparation')
        self.username = "test_admin"
        self.password = User.objects.make_random_password()
        user, created = User.objects.get_or_create(username=self.username)
        user.set_password(self.password)
        user.is_staff = True
        user.is_superuser = True
        user.is_active = True
        user.save()
        self.user = user
    
    def test_process(self):
        filepath = os.path.join(self.files, 'example.zip')
        f = File(open(filepath, 'rb'))
        piup = pi_uploads.objects.create()
        piup.data_file.save('example.zip', f)
        self.assertFalse(piup.processed, "Processed flag not initialized with proper default of False.")
        c = Client()
        c.login(username=self.username, password=self.password)
        response = c.get(reverse("admin:rest_pi_uploads_process", args=[piup.id]), follow=True)
        self.assertRedirects(response, reverse("admin:rest_pi_uploads_change", args=[piup.id]))
        piup.refresh_from_db()
        self.assertTrue(piup.processed, "Processed flag not changed after processing.")
        self.assertEqual(piup.processing_errors, "[KeyError('time',), KeyError('bcc_raw',), KeyError('bcc_raw',), KeyError('time',), KeyError('time',)]", "Processing errors incorrect. Actual processing errors: {}".format(piup.processing_errors))
        