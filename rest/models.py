from decimal import  *
import time, math
import zipfile

from django.contrib.gis.db import models
from django.core.exceptions import ValidationError
from django.utils.timezone import localtime
from pytz import timezone
import pytz

from bike_sensors_web import settings

def validate_zip_file(value):
    if not value.name.endswith('.zip'):
        raise ValidationError(u'Invalid extension. Must be .zip.')
    
    try:
        zipfile.ZipFile(value)
    except zipfile.BadZipfile:
        raise ValidationError(u'Corrupted zipfile.')

class path(models.Model):
    """Pre-defined route a bicycle will be ridden along
    """
    shape = models.LineStringField()
    name = models.CharField(max_length=250)
    
    
class pi_uploads(models.Model):    
    data_file = models.FileField(upload_to='runs/', validators=[validate_zip_file])
    date_uploaded = models.DateTimeField(auto_now_add=True)
    processed = models.BooleanField(default=False)
    processing_errors = models.TextField(blank=True)
    
    def __str__(self):
        return str(localtime(self.date_uploaded)) + str(' Unprocessed' if not self.processed else '' )
    
class run(models.Model):
    """An instance of data collection 
    """
    path = models.ForeignKey(path, null=True, blank=True)
    uuid = models.CharField(max_length=36, unique=True)
    upload = models.ForeignKey(pi_uploads)
    ignore = models.BooleanField(default=False)
    
    def first_timestamp(self, return_as_datetime=False):
        try:
            if self.recording_set.count() == 0:
                return None
            if return_as_datetime:
                return self.recording_set.order_by("measurement__timestamp").first().measurement_set.order_by("timestamp").first().timestamp
            return self.recording_set.order_by("measurement__timestamp").first().measurement_set.order_by("timestamp").first().timestamp.astimezone(timezone(settings.TIME_ZONE)).strftime('%Y.%m.%d %H:%M:%S')

        except AttributeError as e:
            return None
    
    def last_timestamp(self, return_as_datetime=False):
        try:
            if return_as_datetime:
                return self.recording_set.order_by("computer_timestamp").last().computer_timestamp
            return self.recording_set.order_by("computer_timestamp").last().computer_timestamp_compact()
        except AttributeError as e:
            return None
    
    def __str__(self):
        return "id {} from {} to {}".format(self.id, 
                                    self.first_timestamp() if self.first_timestamp() else "no measurements", 
                                    self.last_timestamp() if self.last_timestamp() else "no measurements")
    
class recording(models.Model):
    """Collection of measurements at a particular second
    """
    run = models.ForeignKey(run)
    computer_timestamp = models.DateTimeField()
    
    def computer_timestamp_seconds(self):
        return time.mktime(self.computer_timestamp.timetuple())
    
    def computer_timestamp_compact(self):
        return self.computer_timestamp.astimezone(timezone(settings.TIME_ZONE)).strftime('%Y.%m.%d %H:%M:%S')
    
    class Meta:
        ordering = ['computer_timestamp']
        unique_together = ['run', 'computer_timestamp']


class sensor(models.Model):
    """A sensor attached to the pi.
    """
    CPC = 'cpc'
    AETH = 'aeth'
    GPS = 'gps'
    
    #This category tuple's first value must be the model name
    CATEGORIES = (
                  (CPC, 'CPC'),
                  (AETH, 'Aeth'),
                  (GPS, 'GPS'),
                  )
    serial = models.CharField(max_length=250)
    category = models.CharField(max_length=5, choices=CATEGORIES)
    alpha = models.DecimalField(max_digits=11, decimal_places=9, default=Decimal(1.0), help_text="FOR CPC USE: Use this field for intra-device corrections. Used as: y = alpha * x ; where y is corrected concentration and x is the uncorrected (3007 03) concentration. FOR AETH USE: Use this field for intra-device corrections. Used as: y = alpha * x; where y is corrected concentration and x is the S6 970 concentration.")
    beta = models.DecimalField(max_digits=11, decimal_places=9, default=Decimal(1.0), help_text="FOR CPC USE: Use this field for correcting to base station measurement. Used as: y = alpha * x ; where y is corrected concentration and x is the uncorrected (3007 03) concentration.")
    
    def __str__(self):
        return "{}: {}".format(self.category, self.serial)
    
    class Meta:
        unique_together = (("serial", "category"))
    
    
class measurement(models.Model):
    """Base class for all measurements
    """
    recording = models.ForeignKey(recording)
    timestamp = models.DateTimeField()
    sensor = models.ForeignKey(sensor)
    
    def timestamp_compact(self):
        return self.timestamp.astimezone(timezone(settings.TIME_ZONE)).strftime('%Y.%m.%d %H:%M:%S')

class approximate_location(models.Model):
    '''
    Base class to add a link to the GPS table for an approximate location (to handle time adjusted measurements)
    '''
    gps_measurement = models.ForeignKey('gps', null=True) # b/c of the time offset, there may not be an appropriate GPS location
    
    class Meta:
        abstract = True


class abstract_cpc_values(models.Model):
    ppcc_raw = models.IntegerField()
    ppcc_rectified = models.DecimalField(max_digits=20, decimal_places=10, null=True)
    ppcc = models.DecimalField(max_digits=20, decimal_places=10, null=True)
    
    def calc_ppcc(self):
        # Correct issues between CPCs using empirically determined alpha value 
        ppcc_rectified = Decimal(self.sensor.alpha) * Decimal(self.ppcc_raw)
        
        # Correct to base station using empirically determined beta value
        ppcc_rectified = Decimal(self.sensor.beta) * ppcc_rectified
        self.ppcc_rectified = ppcc_rectified
        # See Apte 2011 eq 4; from Westerdahl et al., 2005
        ppcc_corrected = ppcc_rectified
        if self.ppcc_raw > 10**5:
            exp_inside = Decimal(ppcc_rectified) * Decimal(10**-5)
            exp_result = Decimal(math.exp(exp_inside))
            ppcc_corrected = Decimal(38456.0) * exp_result
            
        self.ppcc=ppcc_corrected
        
        self.save()
    
    class Meta:
        abstract = True
        
        
class abstract_aeth_values(models.Model):
    # Raw measurements
    raw_reference = models.IntegerField()
    raw_sensor = models.IntegerField()
    error = models.BooleanField(default=False)
    flow = models.FloatField()
    
    # Calculations for mechanical shock results
    bcc_ld30 = models.DecimalField(max_digits=38, decimal_places=28, null=True)
    p25 = models.DecimalField(max_digits=38, decimal_places=28, null=True)
    p75 = models.DecimalField(max_digits=38, decimal_places=28, null=True)
    cev = models.BooleanField(default=False)
    so = models.BooleanField(default=False)
    atn = models.DecimalField(max_digits=38, decimal_places=28, null=True)
    
    # Adjustments/corrections/derived values
    bcc_raw = models.DecimalField(max_digits=38, decimal_places=28, null=True)
    bcc_corrected = models.DecimalField(max_digits=38, decimal_places=28, null=True)
    bcc_rectified = models.DecimalField(max_digits=38, decimal_places=28, null=True)
    
    class Meta:
        abstract = True

    
class cpc(measurement, approximate_location, abstract_cpc_values):
    """Measurement from the Condensation Particle Counter
    """


class aeth(measurement, approximate_location, abstract_aeth_values):
    """Measurement from the microAeth
    """


class background_cpc(abstract_cpc_values):
    timestamp = models.DateTimeField(db_index=True)
    sensor = models.ForeignKey(sensor)
    ppcc_1h_smoothed = models.DecimalField(max_digits=20, decimal_places=10, null=True)


class background_aeth(abstract_aeth_values):
    timestamp = models.DateTimeField(db_index=True)
    sensor = models.ForeignKey(sensor)
    bcc_1h_smoothed = models.DecimalField(max_digits=20, decimal_places=10, null=True)
    
    
class gps(measurement):
    """Measurement from the GPS
    """
    point = models.PointField()
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    