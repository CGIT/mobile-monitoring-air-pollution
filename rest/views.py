import csv
from datetime import timedelta
import json

from django.db.models.aggregates import Avg
from django.http import HttpResponseNotAllowed, JsonResponse
from django.shortcuts import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from rest.forms import UploadDataForm
from rest.models import run, recording, path, measurement, gps, sensor, background_aeth, background_cpc


@csrf_exempt
def upload(request):
    if request.method == "POST": 
        form = UploadDataForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return HttpResponse("success")
        else:
            status_code = 422
            if 'data_file' in form.errors:
                status_code = 415
            return HttpResponse(status=status_code, content=json.dumps(form.errors))    
    else:
        return HttpResponseNotAllowed(["POST"])

def paths(request):
    paths_json_serializable = {}
    for p in path.objects.all():
        paths_json_serializable[p.name] = str(p.shape)
    
    return JsonResponse(paths_json_serializable)

def gps_points(request, runid):
    rn = run.objects.get(id=runid)
    g_set = gps.objects.filter(recording__run=rn)
    
    gps_json_serializable = {}
    for g in g_set:
        cpc_value = None
        aeth_value = None
        for m in g.recording.measurement_set.all():
            if m.sensor.category == "cpc":
                cpc_value = m.cpc.ppcc
            if m.sensor.category == sensor.AETH:
                aeth_value = m.aeth.bcc
        gps_json_serializable[g.id] = {'shape':str(g.point.wkt), 'ppcc':cpc_value, 'bcc':aeth_value}
    
    return JsonResponse(gps_json_serializable)

def measurements(request, runid, out_format='json'):
    rn = run.objects.get(id=runid)
    
    # find end - start
    # divide by 2
    # add back to start
    # extract hour
    rn_start = rn.first_timestamp(return_as_datetime=True)
    rn_end = rn.last_timestamp(return_as_datetime=True)
    difference = rn_end - rn_start
    half = difference / 2
    hour_bin = (rn_start + half).hour
    hour_bin = hour_bin - 4 # This corrects for timezone. Django normally handles this, but doesn't seem to for the hour block filtering.
    
    bcc_avg_background = background_aeth.objects.filter(timestamp__hour__range=(hour_bin,hour_bin+1)).aggregate(average=Avg('bcc_1h_smoothed'))['average']
    if bcc_avg_background:
        bcc_avg_background = bcc_avg_background 
    ppcc_avg_background = background_cpc.objects.filter(timestamp__hour__range=(hour_bin,hour_bin+1)).aggregate(average=Avg('ppcc_1h_smoothed'))['average']
    
    recordings = recording.objects.filter(run=rn).order_by("computer_timestamp")[30:]
    if out_format == 'json':
        gps_json_serializable = []
        for r in recordings:
            cpc_time = None
            cpc_full_time = None
            cpc_value = None
            cpc_loc = None
            
            aeth_time = None
            aeth_full_time = None
            aeth_value = None
            aeth_loc = None
            aeth_so = False
            for m in r.measurement_set.all():
                if m.sensor.category == sensor.CPC:
                    cpc_time = m.timestamp_compact()
                    cpc_full_time = m.timestamp
                    cpc_value = m.cpc.ppcc
                    cpc_loc = m.cpc.gps_measurement
                if m.sensor.category == sensor.AETH:
                    aeth_value = m.aeth.bcc_rectified
                    aeth_time = m.timestamp_compact()
                    aeth_full_time = m.timestamp
                    aeth_loc = m.aeth.gps_measurement
                    aeth_so = m.aeth.so
            
            aeth_value_background = False
            if aeth_full_time and aeth_time:
                nearest_bg_aeth = background_aeth.objects.filter(timestamp__range=(
                    aeth_full_time - timedelta(seconds=30),
                    aeth_full_time + timedelta(seconds=30)
                    )).order_by('timestamp').first()
                if nearest_bg_aeth:
                    aeth_value_background = nearest_bg_aeth.bcc_1h_smoothed
            
            cpc_value_background = False
            if cpc_full_time and cpc_time:
                nearest_bg_cpc = background_cpc.objects.filter(timestamp__range=(
                    cpc_full_time - timedelta(seconds=30),
                    cpc_full_time + timedelta(seconds=30)
                    )).order_by('timestamp').first()
                if nearest_bg_cpc:
                    cpc_value_background = nearest_bg_cpc.ppcc_1h_smoothed
                    
            new_row = {'ppcc_time': cpc_time,
                        'ppcc_value': cpc_value,
                        'ppcc_value_background': cpc_value_background,
                        'ppcc_avg_background': ppcc_avg_background,
                        'ppcc_loc': str(cpc_loc.point.wkt) if cpc_loc else '',
                        'bcc_time': aeth_time,
                        'bcc_value': aeth_value,
                        'bcc_value_background': aeth_value_background,
                        'bcc_avg_background': bcc_avg_background,
                        'bcc_loc': str(aeth_loc.point.wkt) if aeth_loc else '',
                        'bcc_so': aeth_so}
            gps_json_serializable.append(new_row)        
        return JsonResponse(gps_json_serializable, safe=False)
    elif out_format == 'csv':
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="{}.csv"'.format(rn.uuid)
    
        writer = csv.writer(response)
        writer.writerow(['cpc_time',
                            'cpc_loc_x',
                            'cpc_loc_y',
                            'cpc_raw_ppcc',
                            'cpc_rectified_ppcc',
                            'cpc_corrected_ppcc',
                            'instantaneous_backgound_ppcc',
                            'hour_average_background_ppcc',
                            'aeth_time',
                            'aeth_loc_x',
                            'aeth_loc_y',
                            'aeth_raw_reference',
                            'aeth_raw_sensor',
                            'aeth_error',
                            'aeth_flow',
                            'aeth_bcc_ld30',
                            'aeth_p25',
                            'aeth_p75',
                            'aeth_cev',
                            'aeth_so',
                            'aeth_bcc_raw',
                            'aeth_bcc_corrected',
                            'aeth_bcc_rectified',
                            'instantaneous_backgound_bcc',
                            'hour_average_background_bcc', ])
        for r in recordings:
            cpc_time = None
            cpc_full_time = None
            cpc_loc_x = None
            cpc_loc_y = None
            cpc_raw_ppcc = None
            cpc_rectified_ppcc = None
            cpc_corrected_ppcc = None
            
            aeth_time = None
            aeth_full_time = None
            aeth_loc_x = None
            aeth_loc_y = None
            aeth_raw_reference = None
            aeth_raw_sensor = None
            aeth_error = None
            aeth_flow = None
            aeth_bcc_ld30 = None
            aeth_p25 = None
            aeth_p75 = None
            aeth_cev = None
            aeth_so = None
            aeth_bcc_raw = None
            aeth_bcc_corrected = None
            aeth_bcc_rectified = None
            for m in r.measurement_set.all():
                if m.sensor.category == sensor.CPC:
                    cpc_time = m.timestamp_compact()
                    cpc_full_time = m.timestamp
                    cpc_loc_x = m.cpc.gps_measurement.point.x if m.cpc.gps_measurement else ''
                    cpc_loc_y = m.cpc.gps_measurement.point.y if m.cpc.gps_measurement else ''
                    cpc_raw_ppcc = m.cpc.ppcc_raw
                    cpc_rectified_ppcc = m.cpc.ppcc_rectified
                    cpc_corrected_ppcc = m.cpc.ppcc
                if m.sensor.category == sensor.AETH:
                    aeth_time = m.timestamp_compact()
                    aeth_full_time = m.timestamp
                    aeth_loc_x = m.aeth.gps_measurement.point.x if m.aeth.gps_measurement else ''
                    aeth_loc_y = m.aeth.gps_measurement.point.y  if m.aeth.gps_measurement else ''                   
                    aeth_raw_reference = m.aeth.raw_reference
                    aeth_raw_sensor = m.aeth.raw_sensor
                    aeth_error = m.aeth.error
                    aeth_flow = m.aeth.flow
                    aeth_bcc_ld30 = m.aeth.bcc_ld30
                    aeth_p25 = m.aeth.p25
                    aeth_p75 = m.aeth.p75
                    aeth_cev = m.aeth.cev
                    aeth_so = m.aeth.so
                    aeth_bcc_raw = m.aeth.bcc_raw
                    aeth_bcc_corrected = m.aeth.bcc_corrected
                    aeth_bcc_rectified = m.aeth.bcc_rectified
            
            aeth_value_background = None
            if aeth_full_time and aeth_time:
                nearest_bg_aeth = background_aeth.objects.filter(timestamp__range=(
                    aeth_full_time - timedelta(seconds=30),
                    aeth_full_time + timedelta(seconds=30)
                    )).order_by('timestamp').first()
                if nearest_bg_aeth:
                    aeth_value_background = nearest_bg_aeth.bcc_1h_smoothed
            
            cpc_value_background = None
            if cpc_full_time and cpc_time:
                nearest_bg_cpc = background_cpc.objects.filter(timestamp__range=(
                    cpc_full_time - timedelta(seconds=30),
                    cpc_full_time + timedelta(seconds=30)
                    )).order_by('timestamp').first()
                if nearest_bg_cpc:
                    cpc_value_background = nearest_bg_cpc.ppcc_1h_smoothed    
                    
            writer.writerow([cpc_time,
                                cpc_loc_x,
                                cpc_loc_y,
                                cpc_raw_ppcc,
                                cpc_rectified_ppcc,
                                cpc_corrected_ppcc,
                                cpc_value_background,
                                ppcc_avg_background,
                                aeth_time,
                                aeth_loc_x,
                                aeth_loc_y,
                                aeth_raw_reference,
                                aeth_raw_sensor,
                                aeth_error,
                                aeth_flow,
                                aeth_bcc_ld30,
                                aeth_p25,
                                aeth_p75,
                                aeth_cev,
                                aeth_so,
                                aeth_bcc_raw,
                                aeth_bcc_corrected,
                                aeth_bcc_rectified,
                                aeth_value_background,
                                bcc_avg_background, ])
        return response
    else:
        return HttpResponseNotAllowed(["You must select a valid format. {} is not valid".format(format)])
                    
            
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
