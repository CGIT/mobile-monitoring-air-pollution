from django.conf.urls import include, url
from django.contrib import admin
from rest import views

urlpatterns = [
    url(r'^upload/', views.upload, name='upload'),
    url(r'^paths/', views.paths, name='paths'),
    url(r'^measurements/(?P<runid>[0-9]+)', include([
            url(r'^$', views.measurements, name='measurements'),
            url(r'^.(?P<out_format>(json|csv))$', views.measurements, name='measurements')
        ])),
]