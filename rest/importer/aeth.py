import datetime
from decimal import Decimal, getcontext
import json
import math
import traceback

from django.utils import timezone
from django.utils.dateparse import parse_datetime
from numpy.random.mtrand import np
from datetime import timedelta

def transform_file(data_reader, data_header, ru):
    # Turn a file into an array of dicts
    aeth_rows = []
    errors = []
    for row in data_reader:
        try:
            if row[data_header.index("run_uid")] != ru.uuid:
                continue;        
            # Add the measurements to the database
            aeth_dict = json.loads(row[data_header.index("ath")].replace("'", '"'))
            gps_dict = json.loads(row[data_header.index("gps")].replace("'", '"').replace("u\"", '"'))
            aeth_entry = {
                        'raw_timestamp':parse_datetime(row[data_header.index("date")]),
                        'timestamp':parse_datetime(gps_dict["time"]) + datetime.timedelta(0, 8),
                        'raw_reference':Decimal(aeth_dict["ref"]),
                        'raw_sensor':Decimal(aeth_dict["sensor"]),
                        'error':int(aeth_dict["status"]),
                        'flow':Decimal(aeth_dict["flow"]),
                        }
            aeth_rows.append(aeth_entry)  
        except Exception as e:
#             traceback.print_exc()
            errors.append(e)
    return aeth_rows, errors

def transform_file_dat(data_reader, data_header):
    # Turn a file into an array of dicts
    aeth_rows = []
    errors = []
    for row in data_reader:
        try:      
            # Add the measurements to the database
#             print(row[data_header.index("ath")].replace("'", '"'))
            time = timezone.make_aware(datetime.datetime.strptime("{} {}".format(row[data_header.index("Date")], row[data_header.index("Time")] ), "%Y/%m/%d %H:%M:%S"))
            aeth_entry = {
                        'raw_timestamp':time,
                        'timestamp':time,
                        'raw_reference':Decimal(row[data_header.index("Ref")]),
                        'raw_sensor':Decimal(row[data_header.index("Sen")]),
                        'error':int(0),
                        'flow':Decimal(row[data_header.index("Flow")]),
                        }
            aeth_rows.append(aeth_entry) 
        except Exception as e:
#             traceback.print_exc()
            errors.append(e)
    return aeth_rows, errors

def annotate_mechanical_shock(aeth_rows):
    # Apte et. al. 2011 equation 2, 3a, 3b, 4, 5.
    errors = []
    # Compute LD30s
    
#     print("IN MEC SHOCK")
    
    ld30 = TimeMovingAverage(aeth_rows, 'timestamp', 'bcc_raw', 15, 14)
    
#     print("LD CREATED")
    for row in aeth_rows:
#         print("ROW: {}".format(row))
        try:
#             print("raw:{}".format(row['bcc_raw']))
            row['bcc_ld30'] = ld30.getNextAverage()
            row['bcc_deviation'] = row['bcc_raw'] - row['bcc_ld30']
#             print("raw:{} ld:{} dev:{}".format(row['bcc_raw'],row['bcc_ld30'],row['bcc_deviation']))
        except Exception as e:
#             traceback.print_exc()
            errors.append(e)
    del ld30
    
    # Compute percentiles and flag CEV
    p300_25 = TimeMovingPercentile(aeth_rows, 'timestamp', 'bcc_deviation', 150, 149, 25)
    p300_75 = TimeMovingPercentile(aeth_rows, 'timestamp', 'bcc_deviation', 150, 149, 75)
    for row in aeth_rows:
        try:
            row['p25'] = p300_25.getNextPercentile()
            row['p75'] = p300_75.getNextPercentile()
            if row.has_key('bcc_deviation'):
                row['cev'] = (row['bcc_deviation'] > 5 * row['p75']) or (row['bcc_deviation'] < 5 * row['p25'])
                if row['bcc_deviation'] > 20 or row['bcc_deviation'] < -20:
#                     print('Deviation flagged')
                    row['cev'] = True
        except Exception as e:
#             traceback.print_exc()
            errors.append(e)
    del p300_25
    del p300_75
            
    # Flag SO
    so = TimeMovingWindow(aeth_rows, 'timestamp', 2, 2)
    for row in aeth_rows:
        try:
            curIndex, highIndex = so.getNextIndexPair()
            if row.has_key('bcc_raw'):
                start = curIndex
                while start <= highIndex:
                    if aeth_rows[start].has_key('cev') and aeth_rows[start]['cev'] and aeth_rows[start]['bcc_raw'] < 0:
                        row['so'] = True
                        break;
                    else:
                        row['so'] = False
                    start = start + 1
                if row['so']:
#                     print("IGNORE")
                    start = curIndex
                    while start <= highIndex:
                        aeth_rows[start]['so'] = True
                        start = start + 1
                    
        except Exception as e:
#             traceback.print_exc()
            errors.append(e)
    del so
    return errors

def normalize_bcc(sensor1, ref1, ref2):
    return sensor1 * ref2 / ref1

def annotate_bcc(aeth_rows, alpha, beta, ignore_field=None):
    '''
    minutes_to_units_conversion: the value to be used when converting from minutes to another unit (default is 60, to convert from minutes to seconds)
    '''
    prev_row2 = None
    prev_row = None
    errors = []
    ref_smoothed = TimeMovingAverage(aeth_rows, 'timestamp', 'raw_reference', 4, 4, ignore_field)
    sen_smoothed = TimeMovingAverage(aeth_rows, 'timestamp', 'raw_sensor', 4, 4, ignore_field)
    for row in aeth_rows:
        row['ref_smoothed'] = ref_smoothed.getNextAverage()
        row['sen_smoothed'] = sen_smoothed.getNextAverage()
        if (ignore_field is None) or (ignore_field and row.has_key(ignore_field) and not row[ignore_field]):
            try:
                if prev_row2:
    #                 print("starting bcc")
                    # Define constants
                    area = Decimal(7.1*(10.0**-6.0)) # Apte et. al. 2011 Supplemental Info lines 172-173
                    sigma = Decimal(12.5) # Apte et. al. 2011 Supplemental Info lines 174-175
                    
                    # Compute flow (see Apte et. al. 2011 Supplemental Info lines 173-174)                
                    timedifference = row['timestamp'] - prev_row['timestamp']
                    if timedifference.total_seconds() == 0:
                        timedifference = timedelta(seconds=1)
                    volume = (row['flow']*Decimal(timedifference.total_seconds()))/(Decimal(10.0**6.0)*Decimal(60.0))
                    
                    # Compute dATN (see Apte et. al. 2011 Supplemental Info SI.7)
                    normalize_top = normalize_bcc(row['sen_smoothed'], row['ref_smoothed'], prev_row['ref_smoothed'])
                    normalize_bottom = normalize_bcc(prev_row['sen_smoothed'], prev_row['ref_smoothed'], prev_row2['ref_smoothed'])
    #                 print(normalize_top/normalize_bottom)
                    dATN = Decimal(-100.0)*Decimal(normalize_top/normalize_bottom).ln()
                    
                    # Compute bcc (see Apte et. al. 2011 Supplemental Info SI.8) and convert to micrograms/m^3
                    bcc = (area / volume)*((dATN / Decimal(100.0)) / sigma)*Decimal(10.0**6.0)
                    row['bcc_raw'] = bcc
                    
                    # Apte et. al. 2011 equation 3.
                    # Source needs to be provided for the calculation of atn, which is different from dATN
                    # Holding off until we get more data from the manufacturer
    #                 print(row['raw_reference'] / row['raw_sensor'])
                    row['atn'] = Decimal(100.0) * Decimal(row['ref_smoothed'] / row['sen_smoothed']).ln()
                    
                    # correct atn based on instrument (emperically determined)
                    row['atn'] = row['atn'] + beta
                    
                    #Kirchstetter, T.W., Novakov, T., 2007. Controlled generation of black carbon particles from a diffusion flame and applications in evaluating black carbon measurement methods. Atmospheric Environment 41, 1874e1888.
                    #See figure 6b and eq 2 (but with different 0.6; should be 1) left side before "proportional" symbol
                    tr = Decimal(Decimal(-1) * row['atn'] / Decimal(100)).exp() # page 1884
                    bcc_corrected = bcc * (Decimal(0.88) * tr + Decimal(0.12))**Decimal(-1)
                    row['bcc_corrected'] = bcc_corrected
                    
                    # Correct between devices
                    bcc_rectified = alpha * bcc_corrected
                    
                    if row.has_key('bcc_rectified') and row['bcc_rectified'] != bcc_rectified:
                        print("Initial {} Updated {}".format(row['bcc_rectified']), bcc_rectified)
                    
                    row['bcc_rectified'] = bcc_rectified
            except Exception as e:
    #             traceback.print_exc()
                errors.append(e)
            finally:            
                prev_row2 = prev_row
                prev_row = row
    
    return errors
           

class TimeMovingWindow(object):
    def __init__(self, data, datetime_attribute, seconds_before, seconds_after,ignore_field=None):
        self.data, self.datetime_attribute,self.ignore_field = data, datetime_attribute, ignore_field
        
        self.time_before = datetime.timedelta(0, seconds_before)
        self.time_after = datetime.timedelta(0, seconds_after)
        
        self.currentIndex = -1  # corrects off by one error when calling getNextPercentile the first time (first call should be index 0)
        self.lowIndex = 0
        self.highIndex = 0
        self.valueArray = []
    
    @staticmethod
    def _computeAvg(data, lowIndex, highIndex, value_attribute, ignore_field=None):
        range_ = None
        if ignore_field:
            range_ = filter(lambda x: x.has_key(value_attribute) and x.has_key(ignore_field) and not x[ignore_field], data[lowIndex : highIndex + 1])
            ignored_range_ = filter(lambda x: x.has_key(value_attribute) and x.has_key(ignore_field) and x[ignore_field], data[lowIndex : highIndex + 1])
#             print("Ignored {}: {}".format(len(ignored_range_), str(ignored_range_)))
        else:
            range_ = filter(lambda x: x.has_key(value_attribute), data[lowIndex : highIndex + 1])
        
        if len(range_):
            sum_ = reduce(lambda x, y: Decimal(x) + Decimal(y), map(lambda x: x[value_attribute], range_))
        
            average = Decimal(sum_) / Decimal(len(range_))
            return average
        else:
            return None
    
    @staticmethod
    def _nextLowIndex(data, lowIndex, min_date, datetime_attribute):
        while min_date > data[lowIndex][datetime_attribute] and lowIndex < len(data) - 1:
            lowIndex = lowIndex + 1
        return lowIndex
    
    @staticmethod
    def _nextHighIndex(data, highIndex, max_date, datetime_attribute):
        while highIndex < len(data) - 1 and max_date >= data[highIndex + 1][datetime_attribute]:
            highIndex = highIndex + 1
        return highIndex
    
    def getNextIndexPair(self):
        """
        Get the next low and high index pair in the sequence.
        
        The first call returns the low and high index pair for index 0, then continues along the rest of the array.
        
        Returns:
            Tuple: Index zero is the low and index one is the high.
        """
        self.currentIndex = self.currentIndex + 1
        if self.currentIndex == len(self.data):
            return False
        
        min_date = self.data[self.currentIndex][self.datetime_attribute] - self.time_before
        max_date = self.data[self.currentIndex][self.datetime_attribute] + self.time_after

        self.lowIndex = self._nextLowIndex(self.data, self.lowIndex, min_date, self.datetime_attribute)
        
        self.highIndex = self._nextHighIndex(self.data, self.highIndex, max_date, self.datetime_attribute)
        
        return (self.lowIndex, self.highIndex)

        
class TimeMovingPercentile(TimeMovingWindow):
    """
    This class handles a time-based moving percentile and allows setting a custom window of seconds before and after the current timestamp.
    
    This uses numpy's percentile function. This means values are briefly converted to floats for the percentile calculate itself, which may introduce a small amount of error.
    
    Args:
        data (array): An array of dictionary objects. The dictionary objects must all have a datetime and a value attribute.
        datetime_attribute (string): The key corresponding to the datetime entry in each row's dictionary.
        value_attribute (string): The key corresponding to the value entry in each row's dictionary.
        seconds_before (int): The number of seconds to average before. Inclusive.
        seconds_after (int): The number of seconds to average after. Inclusive.
        percentile (float): In range of [0,100] (or sequence of floats). Percentile to compute, which must be between 0 and 100 inclusive.
    """
    def __init__(self, data, datetime_attribute, value_attribute, seconds_before, seconds_after, precentile):
        super(TimeMovingPercentile, self).__init__(data, datetime_attribute, seconds_before, seconds_after)
        self.value_attribute = value_attribute
        self.percentile = precentile
        
    def getNextPercentile(self):
        """
        Get the next percentile in the sequence.
        
        The first call returns the percentile for index 0, then continues along the rest of the array.
        
        Returns:
            Decimal: The time-based moving window percentile for the next row.
        """
        self.currentIndex = self.currentIndex + 1
        
        min_date = self.data[self.currentIndex][self.datetime_attribute] - self.time_before
        max_date = self.data[self.currentIndex][self.datetime_attribute] + self.time_after

        self.lowIndex = self._nextLowIndex(self.data, self.lowIndex, min_date, self.datetime_attribute)
        
        self.highIndex = self._nextHighIndex(self.data, self.highIndex, max_date, self.datetime_attribute)
        
        sub_data = filter(lambda x: x.has_key(self.value_attribute), self.data[self.lowIndex : self.highIndex + 1])
        sub_values = map(lambda x: Decimal(x[self.value_attribute]), sub_data)

        return np.percentile(sub_values, self.percentile, interpolation='lower')


class TimeMovingAverage(TimeMovingWindow):
    """
    This class handles a time-based moving average and allows setting a custom window of seconds before and after the current timestamp.
    
    Args:
        data (array): An array of dictionary objects. The dictionary objects must all have a datetime and a value attribute.
        datetime_attribute (string): The key corresponding to the datetime entry in each row's dictionary.
        value_attribute (string): The key corresponding to the value entry in each row's dictionary.
        seconds_before (int): The number of seconds to average before. Inclusive.
        seconds_after (int): The number of seconds to average after. Inclusive.
    """
    def __init__(self, data, datetime_attribute, value_attribute, seconds_before, seconds_after,ignore_field=None):
        super(TimeMovingAverage, self).__init__(data, datetime_attribute, seconds_before, seconds_after,ignore_field)
        self.value_attribute = value_attribute
        
    def getNextAverage(self):
        """
        Get the next average in the sequence.
        
        The first call returns the average for index 0, then continues along the rest of the array.
        
        Returns:
            Decimal: The time-based average for the next row.
        """
        self.currentIndex = self.currentIndex + 1
        
        min_date = self.data[self.currentIndex][self.datetime_attribute] - self.time_before
        max_date = self.data[self.currentIndex][self.datetime_attribute] + self.time_after

        self.lowIndex = self._nextLowIndex(self.data, self.lowIndex, min_date, self.datetime_attribute)
        
        self.highIndex = self._nextHighIndex(self.data, self.highIndex, max_date, self.datetime_attribute)
        
        return self._computeAvg(self.data, self.lowIndex, self.highIndex, self.value_attribute, self.ignore_field)

    
if __name__ == "__main__":
    print("main")
    now = datetime.datetime.now()
    example = [
        {'datetime': now + datetime.timedelta(0, 0), 'value': 0},
        {'datetime': now + datetime.timedelta(0, 1), 'value': 1},
        {'datetime': now + datetime.timedelta(0, 2), 'value': 2},
        {'datetime': now + datetime.timedelta(0, 3), 'value': 3},
        {'datetime': now + datetime.timedelta(0, 4), 'value': 4},
        {'datetime': now + datetime.timedelta(0, 5), 'value': 5},
        {'datetime': now + datetime.timedelta(0, 10), 'value': 6},
        {'datetime': now + datetime.timedelta(0, 11), 'value': 7},
        {'datetime': now + datetime.timedelta(0, 30), 'value': 8},
        {'datetime': now + datetime.timedelta(0, 40), 'value': 9},
        {'datetime': now + datetime.timedelta(0, 50), 'value': 10},
        ]
    
    mvg_avg = TimeMovingAverage(example, 'datetime', 'value', 7, 0)
    
    print(mvg_avg.getNextAverage() == 0)  # at index 0
    print(mvg_avg.getNextAverage() == 0.5)  # at index 1
    print(mvg_avg.getNextAverage() == 1.0)  # at index 2
    print(mvg_avg.getNextAverage() == 1.5)  # at index 3
    print(mvg_avg.getNextAverage() == 2)  # at index 4
    print(mvg_avg.getNextAverage() == 2.5)  # at index 5
    print(mvg_avg.getNextAverage() == 4.5)  # at index 6
    print(mvg_avg.getNextAverage() == 5.5)  # at index 7
    print(mvg_avg.getNextAverage() == 8)  # at index 8
    print(mvg_avg.getNextAverage() == 9)  # at index 9
    print(mvg_avg.getNextAverage() == 10)  # at index 10
    print("===")
    
    mvg_avg = TimeMovingAverage(example, 'datetime', 'value', 1, 1)
    
    print(mvg_avg.getNextAverage() == 0.5)  # at index 0
    print(mvg_avg.getNextAverage() == 1.0)  # at index 1
    print(mvg_avg.getNextAverage() == 2)  # at index 2
    print(mvg_avg.getNextAverage() == 3)  # at index 3
    print(mvg_avg.getNextAverage() == 4)  # at index 4
    print(mvg_avg.getNextAverage() == 4.5)  # at index 5
    print(mvg_avg.getNextAverage() == 6.5)  # at index 6
    print(mvg_avg.getNextAverage() == 6.5)  # at index 7
    print(mvg_avg.getNextAverage() == 8)  # at index 8
    print(mvg_avg.getNextAverage() == 9)  # at index 9
    print(mvg_avg.getNextAverage() == 10)  # at index 10
    print("===")
    
    mvg_avg = TimeMovingAverage(example, 'datetime', 'value', 0, 0)
    
    print(mvg_avg.getNextAverage() == 0)  # at index 0
    print(mvg_avg.getNextAverage() == 1)  # at index 1
    print(mvg_avg.getNextAverage() == 2)  # at index 2
    print(mvg_avg.getNextAverage() == 3)  # at index 3
    print(mvg_avg.getNextAverage() == 4)  # at index 4
    print(mvg_avg.getNextAverage() == 5)  # at index 5
    print(mvg_avg.getNextAverage() == 6)  # at index 6
    print(mvg_avg.getNextAverage() == 7)  # at index 7
    print(mvg_avg.getNextAverage() == 8)  # at index 8
    print(mvg_avg.getNextAverage() == 9)  # at index 9
    print(mvg_avg.getNextAverage() == 10)  # at index 10
    print("===")
    
    mvg_avg = TimeMovingAverage(example, 'datetime', 'value', 0, 7)
    
    print(mvg_avg.getNextAverage() == 2.5)  # at index 0
    print(mvg_avg.getNextAverage() == 3)  # at index 1
    print(mvg_avg.getNextAverage() == 3.5)  # at index 2
    print(mvg_avg.getNextAverage() == 4.5)  # at index 3
    print(mvg_avg.getNextAverage() == 5.5)  # at index 4
    print(mvg_avg.getNextAverage() == 6)  # at index 5
    print(mvg_avg.getNextAverage() == 6.5)  # at index 6
    print(mvg_avg.getNextAverage() == 7)  # at index 7
    print(mvg_avg.getNextAverage() == 8)  # at index 8
    print(mvg_avg.getNextAverage() == 9)  # at index 9
    print(mvg_avg.getNextAverage() == 10)  # at index 10
    print("===")
    
    
#     print(mvg_avg.getAverageAt(3))
