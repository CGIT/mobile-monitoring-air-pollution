'''
rest.importer.import_background -- shortdesc

rest.importer.import_background is a description

It defines classes_and_methods

@author:     user_name

@copyright:  2017 organization_name. All rights reserved.

@license:    license

@contact:    user_email
@deffield    updated: Updated
'''

'''Load django settings'''

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
import csv
from datetime import datetime
from decimal import Decimal
import os
import sys
import traceback

from django.core.wsgi import get_wsgi_application
from django.utils import timezone

os.environ['DJANGO_SETTINGS_MODULE'] = 'bike_sensors_web.settings'
application = get_wsgi_application()

from rest.importer.aeth import annotate_mechanical_shock, transform_file_dat, annotate_bcc, \
    TimeMovingAverage
from rest.models import sensor, background_cpc, background_aeth




__all__ = []
__version__ = 0.1
__date__ = '2017-05-16'
__updated__ = '2017-05-16'

DEBUG = 1
TESTRUN = 0
PROFILE = 0

class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg

def import_aeth_file(filepath):
    header_at_line = 16 # line number of the header row (start counting at 1)
    serial_at_line = 3 # line number to parse the serial from (start counting at 1)
    
    with open(filepath) as csv_file_handle:
        aeth_reader = csv.reader(csv_file_handle, delimiter=";")
        
        headers = []
        our_sensor = None
        
        # advance to the header row and store headers as an array; ignore header information
        while aeth_reader.line_num < header_at_line:
            curr_line = aeth_reader.next()
            if aeth_reader.line_num == serial_at_line:
                serial = "{}".format(curr_line[0][12:])
                try:
                    our_sensor = sensor.objects.get(serial=serial)
                except sensor.DoesNotExist:
                    print("Sensor with serial {} was not found.".format(serial))
            if aeth_reader.line_num == header_at_line:
                headers = curr_line
                aeth_reader.next() # handle blank line after header
       
        errors = []
        aeth_rows, additional_errors = transform_file_dat(aeth_reader, headers)
        errors.extend(additional_errors)
        additional_errors = annotate_bcc(aeth_rows, our_sensor.alpha, our_sensor.beta)
        errors.extend(additional_errors)
        additional_errors = annotate_mechanical_shock(aeth_rows[2:])
        errors.extend(additional_errors)
        if errors:
            print("Errors encountered during aeth import: {}".format(str(errors)))
        
        # iterate over all data rows
        for row in aeth_rows[2:]:
            row.pop("raw_timestamp") # this keyword is not used when creating a background_aeth model
            row.pop("ref_smoothed")
            row.pop("sen_smoothed")
            background_aeth.objects.create(
                sensor = our_sensor,
                **row
                )


def import_cpc_file(filepath):
    header_at_line = 6 # line number of the header row (start counting at 1)
    serial_at_line = 5 # line number to parse the serial from (start counting at 1)
    
    with open(filepath) as csv_file_handle:
        cpc_reader = csv.reader(csv_file_handle)
        
        headers = []
        our_sensor = None
        
        # advance to the header row and store headers as an array; ignore header information
        while cpc_reader.line_num < header_at_line:
            curr_line = cpc_reader.next()
            if cpc_reader.line_num == serial_at_line:
                serial = "{}-{}".format(curr_line[0][24:28], curr_line[0][28:])
                try:
                    our_sensor = sensor.objects.get(serial=serial)
                except sensor.DoesNotExist:
                    print("Sensor with serial {} was not found.".format(serial))
            if cpc_reader.line_num == header_at_line:
                headers = curr_line
        
        
        # iterate over all data rows
        for row in cpc_reader:
            line = dict(zip(headers, row))
            time = timezone.make_aware(datetime.strptime("{} {}".format(line['Date'], line['Time'] ), "%Y/%m/%d %H:%M:%S"))

            c = background_cpc.objects.create(
                timestamp = time,
                sensor = our_sensor,
                ppcc_raw = Decimal(line['Concentration'])
                )
            c.calc_ppcc()    

def main(argv=None): # IGNORE:C0111    
    '''Command line options.'''
    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by user_name on %s.
  Copyright 2017 organization_name. All rights reserved.

  Licensed under the Apache License 2.0
  http://www.apache.org/licenses/LICENSE-2.0

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument("directory", help="Path to the directory of CSV files to import")
        parser.add_argument("unittype", help="One of [aeth|cpc]")

        # Process arguments
        args = parser.parse_args()

        directory = args.directory
        unittype = args.unittype

        print("Beginning import of {} data from directory {}".format(unittype, directory))
        
        if not os.path.isdir(directory):
            raise CLIError("path is not a directory")
        
        if unittype == 'aeth':
            # Import all the files
            file_paths_to_import = os.listdir(directory)
            for path in file_paths_to_import:
                print("Importing aeth file {}".format(path))
                import_aeth_file(os.path.join(directory, path))
            
            # Apply 1 hour smoothing
            print("Beginning smoothing calculations")
            aeth_rows = background_aeth.objects.order_by('timestamp')
            aeth_avg = TimeMovingAverage(aeth_rows.values('timestamp', 'bcc_rectified'), 'timestamp', 'bcc_rectified', 1800, 1799)
            for row in aeth_rows:
                try:
                    row.bcc_1h_smoothed = aeth_avg.getNextAverage()
                    row.save(update_fields=["bcc_1h_smoothed"])
                except Exception as e:
                    traceback.print_exc()
        elif unittype == 'cpc':
            # Import all the files
            file_paths_to_import = os.listdir(directory)
            for path in file_paths_to_import:
                print("Importing cpc file {}".format(path))
                import_cpc_file(os.path.join(directory, path))
           
            # Apply 1 hour smoothing
            print("Beginning smoothing calculations")
            cpc_rows = background_cpc.objects.order_by('timestamp')
            cpc_avg = TimeMovingAverage(cpc_rows.values('timestamp', 'ppcc'), 'timestamp', 'ppcc', 1800, 1799)
            for row in cpc_rows:
                try:
                    row.ppcc_1h_smoothed = cpc_avg.getNextAverage()
                    row.save(update_fields=["ppcc_1h_smoothed"])
                except Exception as e:
                    traceback.print_exc()
        else:
            raise CLIError("unittype not one of [aeth|cpc]")
        
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception, e:
        if DEBUG or TESTRUN:
            raise(e)
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        sys.stderr.write(indent + "  for help use --help")
        return 2
    
    print("Done executing")

if __name__ == "__main__":
    if DEBUG:
        sys.argv.append(r"C:\Users\pipes161\workspace\bikesensor_web\rest\importer\test_data\cpc")
        sys.argv.append("cpc")
    if TESTRUN:
        import doctest
        doctest.testmod()
    if PROFILE:
        import cProfile
        import pstats
        profile_filename = 'rest.importer.import_background_profile.txt'
        cProfile.run('main()', profile_filename)
        statsfile = open("profile_stats.txt", "wb")
        p = pstats.Stats(profile_filename, stream=statsfile)
        stats = p.strip_dirs().sort_stats('cumulative')
        stats.print_stats()
        statsfile.close()
        sys.exit(0)
    sys.exit(main())