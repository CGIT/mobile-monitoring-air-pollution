'''
Created on Mar 9, 2017

@author: pipes161
'''
import os

from django.db.models.aggregates import Avg


if __name__ == '__main__':
    
    from django.core.wsgi import get_wsgi_application

    os.environ['DJANGO_SETTINGS_MODULE'] = 'bike_sensors_web.settings'
    application = get_wsgi_application()
    
    from rest.models import pi_uploads, run
    from rest.importer.tool import DataImporter
    
    print("Deleting all past runs for re-import")
    run.objects.all().delete()
    
    for upload in pi_uploads.objects.all().order_by('id'): #.filter(id=69)
        print("Working on upload {}, file {}, uploaded {}".format(upload.id, upload.data_file.name, upload.date_uploaded))
        
        i = DataImporter(upload)
        i.open_sensor_log()
        errors = i.import_now()
        i.release()
        
        upload.processed=True
        if errors:
            upload.processing_errors=errors
        upload.save()




