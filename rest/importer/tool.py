'''
Created on Sep 18, 2015

@author: pipes161
'''

import csv, collections, numpy
import datetime
import json
from tempfile import NamedTemporaryFile
import traceback
from zipfile import ZipFile

from django.contrib.gis.geos import Point
from django.utils.dateparse import parse_datetime

from rest.importer.aeth import annotate_mechanical_shock, transform_file, \
    annotate_bcc
from rest.models import path, run, recording, cpc, aeth, sensor, gps, pi_uploads


class MovingAverage():
    """
    Object that handles calculating a moving average by maintaining a deque of numbers.
    
    Uses the module collections.deque. 
    """    
    def __init__(self, size=10):
        """       
        size is the maxmimum number of values to average (the window size of the moving average). 
        """
        self.q = collections.deque(maxlen=size)
        self.size = size
        
    def getAverage(self, newValue):
        """Returns the average of the most recent n objects, where n is the size passed during object initilization. 
        
        This method combines adding a new value to the moving average and computing the average.
        
        newValue is the value to be added to the deque. 
        """
        if len(self.q) == self.q.maxlen:
            self.q.popleft()
        self.q.append(float(newValue))
        avg = numpy.mean(list(self.q))
        return avg


class DataImporter(object):    
    def __init__(self, pi_upload):
        self.zipfile_path = pi_upload.data_file.path
        self.pi_upload = pi_upload

    def open_sensor_log(self):
        self.data_handle = NamedTemporaryFile()
        self.runs_handle = NamedTemporaryFile()
        with ZipFile(self.zipfile_path) as data_zip:
            datafile = data_zip.open(data_zip.namelist()[0])
            
            passed_runs_header = False
            passed_data_header = False
            
            for row in datafile:
                if 'date' in row and not passed_runs_header:
                    self.runs_handle.write(row)
                    passed_runs_header = True
                elif 'date' in row and not passed_data_header:
                    self.data_handle.write(row)
                    passed_data_header = True
                elif passed_runs_header and not passed_data_header:
                    self.runs_handle.write(row)
                elif passed_runs_header and passed_data_header:
                    self.data_handle.write(row)
                else:
                    raise ValueError("Something went wrong splitting the file.")
    
    def release(self):
        self.data_handle.close()
        self.runs_handle.close()

    def import_now(self): 
        errors = []
        # Sensor and path selection.
        
        # Begin data import
        self.data_handle.seek(0)
        self.runs_handle.seek(0)
        
        data_reader = csv.reader(self.data_handle)
        runs_reader = csv.reader(self.runs_handle)
        
        runs_header = next(runs_reader)
        
        # Add data from the runs file to the database
        for run_row in runs_reader:
            try:
                ru = run.objects.create(uuid=run_row[runs_header.index("run_uid")], upload=self.pi_upload)
                self.data_handle.seek(0)
                data_header = next(data_reader)
                ref_moving_average = MovingAverage(size=1)
                sensor_moving_average = MovingAverage(size=1)
                
                try:
                    
                    # Annotate aeth measurements with shock correction and bcc calculations
                    our_aeth, created = sensor.objects.get_or_create(serial=run_row[runs_header.index("ath_serial")], category=sensor.AETH)
                    
                    # Convert the file into a usable format for the rest of the code (array of dicts)
                    aeth_rows, additional_errors = transform_file(data_reader, data_header, ru)
                    errors.extend(additional_errors)
                    
                    # Calculate initial BC values for mechanical shock detection
                    additional_errors = annotate_bcc(aeth_rows, our_aeth.alpha, our_aeth.beta)
                    errors.extend(additional_errors)
                    
                    # Flag spurious observations
                    additional_errors = annotate_mechanical_shock(aeth_rows)
                    errors.extend(additional_errors)
                    
                    # Re-calculate BC, ignoring rows that were flagged as spurious observations
                    additional_errors = annotate_bcc(aeth_rows, our_aeth.alpha, our_aeth.beta, ignore_field='so')
                    errors.extend(additional_errors)
                            
                except Exception as e:
                    errors.append(e)
                    traceback.print_exc()
                
                # Add data from the data file to the database
                our_aeth, created = sensor.objects.get_or_create(serial=run_row[runs_header.index("ath_serial")], category=sensor.AETH)
                self.data_handle.seek(0)
                data_header = next(data_reader)
                our_gps, created = sensor.objects.get_or_create(serial=run_row[runs_header.index("gps_serial")], category=sensor.GPS)
                for row in data_reader:
                    if row[data_header.index("run_uid")] != ru.uuid:
                        continue;
                    
                    try:
                        gps_dict = json.loads(row[data_header.index("gps")].replace("'", '"').replace('u"', '"'))
                        rec = recording.objects.create(run=ru, computer_timestamp=row[data_header.index("date")])
                        gps_dict = json.loads(row[data_header.index("gps")].replace("'", '"').replace('u"', '"'))
                        gps.objects.create(
                                                timestamp=parse_datetime(gps_dict["time"]) + datetime.timedelta(0, 0),
                                                recording=rec,
                                               point=Point(float(gps_dict["lon"]),
                                                           float(gps_dict["lat"])),
                                               sensor=our_gps)
                    except Exception as e:
#                         traceback.print_exc()
                        errors.append(e)
                 
                timedelta_gpslookup = datetime.timedelta(0, 3)
                 
                self.data_handle.seek(0)
                data_header = next(data_reader)
                our_cpc, created = sensor.objects.get_or_create(serial=run_row[runs_header.index("cpc_serial")], category=sensor.CPC)
                for row in data_reader:
                    if row[data_header.index("run_uid")] != ru.uuid:
                        continue;
                    # Add the measurements to the database
                    try:
                        gps_dict = json.loads(row[data_header.index("gps")].replace("'", '"').replace('u"', '"'))
                        rec = recording.objects.get(run=ru, computer_timestamp=row[data_header.index("date")])
                        cpc_dict = json.loads(row[data_header.index("cpc")].replace("'", '"'))
                        c = cpc.objects.create(
                                                timestamp=parse_datetime(gps_dict["time"]) + datetime.timedelta(0, 2),
                                                recording=rec,
                                               ppcc_raw=cpc_dict["ppcc"],
                                               sensor=our_cpc)
                        c.calc_ppcc()
                         
                        nearby_gps = gps.objects.filter(timestamp__range=[c.timestamp - timedelta_gpslookup, c.timestamp + timedelta_gpslookup])
#                         print(len(nearby_gps))
                        actual_gps = None
                        timedistance = timedelta_gpslookup + timedelta_gpslookup + timedelta_gpslookup
                        for g in nearby_gps:
                            new_timedistance = abs(g.timestamp - c.timestamp)
                            if new_timedistance < timedistance:
                                actual_gps = g
                                timedistance = new_timedistance
#                         print(actual_gps.point)
#                         print(timedistance)
                         
                        c.gps_measurement = actual_gps
                         
                        c.save()
                    except Exception as e:
#                         traceback.print_exc()
                        errors.append(e)
                         
                for row in aeth_rows:
                    try:
                        rec = recording.objects.get(run=ru, computer_timestamp=row.pop('raw_timestamp', None))
                         
                        nearby_gps = gps.objects.filter(timestamp__range=[row['timestamp'] - timedelta_gpslookup, row['timestamp'] + timedelta_gpslookup]).order_by('timestamp')
                         
#                         print(len(nearby_gps))
#                         if len(nearby_gps):
#                             print("A: {} | L: {} | H: {}".format(row['timestamp'], nearby_gps[0].timestamp, nearby_gps[len(nearby_gps)- 1].timestamp))
                        actual_gps = None
                        timedistance = (timedelta_gpslookup + timedelta_gpslookup + timedelta_gpslookup)
#                         print("td: {}".format(timedistance))
                        for g in nearby_gps:
                            new_timedistance = None
                            new_timedistance = abs((g.timestamp - row['timestamp']))
#                             print("ntd: {}".format(new_timedistance))
                            if new_timedistance < timedistance:
#                                 print("new time")
                                actual_gps = g
                                timedistance = new_timedistance
                        if row.has_key("ref_smoothed"):
                            row.pop("ref_smoothed")
                        if row.has_key("sen_smoothed"):
                            row.pop("sen_smoothed")
                        if row.has_key("ignore"):
                            row.pop("ignore")
                        if row.has_key("bcc_deviation"):
                            row.pop("bcc_deviation")

                        a = aeth.objects.create(
                                            recording=rec,
                                            sensor=our_aeth,
                                            gps_measurement=actual_gps,
                                            **row)
                    except Exception as e:
#                         traceback.print_exc()
                        errors.append(e)
                    
            except Exception as e:
                traceback.print_exc()
                errors.append(e)
                
        
        
        
        return errors
