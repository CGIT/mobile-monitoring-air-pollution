'''
Created on Mar 9, 2017

@author: pipes161
'''
import csv
from datetime import timedelta
from decimal import Decimal
import os
import tempfile

from django.db.models.aggregates import Avg


if __name__ == '__main__':
    
    from django.core.wsgi import get_wsgi_application

    os.environ['DJANGO_SETTINGS_MODULE'] = 'bike_sensors_web.settings'
    application = get_wsgi_application()
    
    
    from rest.importer.aeth import TimeMovingAverage
    from rest.models import background_aeth, background_cpc
    
    from rest.models import run, recording, sensor
    
    out_cpc_file = tempfile.NamedTemporaryFile(prefix="cpc_", suffix=".csv", delete=False)
    out_aeth_file = tempfile.NamedTemporaryFile(prefix="aeth_", suffix=".csv", delete=False)
    print("CPC measurements will be saved to {}".format(out_cpc_file.name))
    print("Aeth measurements will be saved to {}".format(out_aeth_file.name))
    cpc_writer = csv.writer(out_cpc_file)
    aeth_writer = csv.writer(out_aeth_file)
    
    cpc_writer.writerow(['cpc_time',
                         'cpc_hour_bin_corrected_timezone',
                        'cpc_loc_x',
                        'cpc_loc_y',
                        'cpc_raw_ppcc',
                        'cpc_rectified_ppcc',
                        'cpc_corrected_ppcc',
                        'instantaneous_backgound_ppcc',
                        'hour_average_background_ppcc',
                        'cpc_normalized_ppcc',])
    aeth_writer.writerow(['aeth_time',
                         'aeth_hour_bin_corrected_timezone',
                        'aeth_loc_x',
                        'aeth_loc_y',
                        'aeth_raw_reference',
                        'aeth_raw_sensor',
                        'aeth_error',
                        'aeth_flow',
                        'aeth_bcc_ld30',
                        'aeth_p25',
                        'aeth_p75',
                        'aeth_cev',
                        'aeth_so',
                        'aeth_bcc_raw',
                        'aeth_bcc_corrected',
                        'aeth_bcc_rectified', 
                        'instantaneous_backgound_bcc',
                        'hour_average_background_bcc',
                        'aeth_normalized_bcc',
                        'aeth_bcc_60s_smoothed_normalized',
                        'aeth_bcc_60s_smoothed_not_normalized',])
    all_valid_runs = run.objects.filter(ignore=False) #.filter(upload_id=69)
    print("{} Runs to save to file".format(all_valid_runs.count()))
    run_count = 0
    for rn in all_valid_runs:
        rn_start = rn.first_timestamp(return_as_datetime=True)
        rn_end = rn.last_timestamp(return_as_datetime=True)
        difference = rn_end - rn_start
        half = difference / 2
        hour_bin = (rn_start + half).hour
        hour_bin = hour_bin - 4 # This corrects for timezone. Django normally handles this, but doesn't seem to for the hour block filtering.
        
        print("START {}        END {}        BIN {}        HALF {}".format(rn_start, rn_end, hour_bin, half))
        
        bcc_avg_background = background_aeth.objects.filter(timestamp__hour__range=(hour_bin,hour_bin+1)).aggregate(average=Avg('bcc_1h_smoothed'))['average']
        if bcc_avg_background:
            bcc_avg_background = Decimal(bcc_avg_background )
        ppcc_avg_background = Decimal(background_cpc.objects.filter(timestamp__hour__range=(hour_bin,hour_bin+1)).aggregate(average=Avg('ppcc_1h_smoothed'))['average'])
        
        
        run_count = run_count + 1
        print("Working on run id {} | {}/{}".format(rn.id, run_count, all_valid_runs.count()))
        
        recordings = recording.objects.filter(run=rn).order_by("computer_timestamp")
        if len(recordings) <= 30:
             print("Not enough measurements in this run")
             continue
        
        # First, build an array with all values for the run
        recordings = recordings[30:]
        this_runs_cpc_values = []
        this_runs_aeth_values = []
        for r in recordings:
            cpc_time = None
            cpc_full_time = None
            aeth_time = None
            aeth_full_time = None
            aeth_value_background = False
            
            cpc_values = {
                'cpc_time' : None,
                'cpc_hour_bin_corrected_timezone': hour_bin,
                'cpc_loc_x' : None,
                'cpc_loc_y' : None,
                'cpc_raw_ppcc' : None,
                'cpc_rectified_ppcc' : None,
                'cpc_corrected_ppcc' : None,
                'instantaneous_backgound_ppcc' : None,
                'hour_average_background_ppcc' : None,
                'cpc_normalized_ppcc' : None,
            }
            aeth_values = {
                'aeth_time' : None,
                'aeth_hour_bin_corrected_timezone': hour_bin,
                'aeth_loc_x' : None,
                'aeth_loc_y' : None,
                'aeth_raw_reference' : None,
                'aeth_raw_sensor' : None,
                'aeth_error' : None,
                'aeth_flow' : None,
                'aeth_bcc_ld30' : None,
                'aeth_p25' : None,
                'aeth_p75' : None,
                'aeth_cev' : None,
                'aeth_so' : None,
                'aeth_bcc_raw' : None,
                'aeth_bcc_corrected' : None,
                'aeth_bcc_rectified' : None,
                'aeth_normalized_bcc': None,
                'aeth_bcc_60s_smoothed_normalized': None,
                'aeth_bcc_60s_smoothed_not_normalized': None,
                'instantaneous_backgound_bcc' : None,
                'hour_average_background_bcc': None,
            }
            for m in r.measurement_set.all():
                if m.sensor.category == sensor.CPC:
                    cpc_time = m.timestamp_compact()
                    cpc_full_time = m.timestamp
                    cpc_value = m.cpc.ppcc
                    cpc_loc = m.cpc.gps_measurement
                    cpc_value_background = False
                    
                    if cpc_full_time and cpc_time:
                        nearest_bg_cpc = background_cpc.objects.filter(timestamp__range=(
                            cpc_full_time - timedelta(seconds=30),
                            cpc_full_time + timedelta(seconds=30)
                            )).order_by('timestamp').first()
                        if nearest_bg_cpc:
                            cpc_value_background = nearest_bg_cpc.ppcc_1h_smoothed
                            
                    cpc_values['cpc_time'] = m.timestamp
                    cpc_values['cpc_loc_x'] = m.cpc.gps_measurement.point.x if m.cpc.gps_measurement else ''
                    cpc_values['cpc_loc_y'] = m.cpc.gps_measurement.point.y if m.cpc.gps_measurement else ''
                    cpc_values['cpc_raw_ppcc'] = m.cpc.ppcc_raw
                    cpc_values['cpc_rectified_ppcc'] = m.cpc.ppcc_rectified
                    cpc_values['cpc_corrected_ppcc'] = m.cpc.ppcc
                    cpc_values['instantaneous_backgound_ppcc'] = cpc_value_background if cpc_value_background != False else ''
                    cpc_values['hour_average_background_ppcc'] = ppcc_avg_background
                    if cpc_value_background != False:
                        cpc_values['cpc_normalized_ppcc'] = cpc_values['cpc_corrected_ppcc'] - cpc_values['instantaneous_backgound_ppcc'] + cpc_values['hour_average_background_ppcc']
                    else:
                        cpc_values['cpc_normalized_ppcc'] = cpc_values['cpc_corrected_ppcc']
                    
                    this_runs_cpc_values.append(cpc_values)
                if m.sensor.category == sensor.AETH and not m.aeth.so and m.aeth.bcc_rectified >= -1000 and m.aeth.bcc_rectified <= 1000:
                    aeth_value = m.aeth.bcc_rectified
                    aeth_time = m.timestamp_compact()
                    aeth_full_time = m.timestamp
                    aeth_loc = m.aeth.gps_measurement
                    aeth_so = m.aeth.so
                    
                    if aeth_full_time and aeth_time:
                        nearest_bg_aeth = background_aeth.objects.filter(timestamp__range=(
                            aeth_full_time - timedelta(seconds=30),
                            aeth_full_time + timedelta(seconds=30)
                            )).order_by('timestamp').first()
                        if nearest_bg_aeth:
                            aeth_value_background = nearest_bg_aeth.bcc_1h_smoothed
                    
                    aeth_values['aeth_time'] = m.timestamp
                    aeth_values['aeth_loc_x'] = m.aeth.gps_measurement.point.x if m.aeth.gps_measurement else ''
                    aeth_values['aeth_loc_y'] = m.aeth.gps_measurement.point.y  if m.aeth.gps_measurement else ''                   
                    aeth_values['aeth_raw_reference'] = m.aeth.raw_reference
                    aeth_values['aeth_raw_sensor'] = m.aeth.raw_sensor
                    aeth_values['aeth_error'] = m.aeth.error
                    aeth_values['aeth_flow'] = m.aeth.flow
                    aeth_values['aeth_bcc_ld30'] = m.aeth.bcc_ld30
                    aeth_values['aeth_p25'] = m.aeth.p25
                    aeth_values['aeth_p75'] = m.aeth.p75
                    aeth_values['aeth_cev'] = m.aeth.cev
                    aeth_values['aeth_so'] = m.aeth.so
                    aeth_values['aeth_bcc_raw'] = m.aeth.bcc_raw
                    aeth_values['aeth_bcc_corrected'] = m.aeth.bcc_corrected
                    aeth_values['aeth_bcc_rectified'] = m.aeth.bcc_rectified
                    aeth_values['instantaneous_backgound_bcc'] = aeth_value_background if aeth_value_background != False else ''
                    aeth_values['hour_average_background_bcc'] = bcc_avg_background
                    if aeth_value_background != False:
                        aeth_values['aeth_normalized_bcc'] = aeth_values['aeth_bcc_rectified'] - aeth_values['instantaneous_backgound_bcc'] + aeth_values['hour_average_background_bcc']
                    else:
                        aeth_values['aeth_normalized_bcc'] = aeth_values['aeth_bcc_rectified']
                    
                    this_runs_aeth_values.append(aeth_values)
        
        # Then, annotate with bcc smoothing   
        bcc_60s_smoothing_normalized = TimeMovingAverage(this_runs_aeth_values, 'aeth_time', 'aeth_normalized_bcc', 30, 29)
        bcc_60s_smoothing_not_normalized = TimeMovingAverage(this_runs_aeth_values, 'aeth_time', 'aeth_bcc_rectified', 30, 29)
        for value in this_runs_aeth_values:
            value['aeth_bcc_60s_smoothed_normalized'] = bcc_60s_smoothing_normalized.getNextAverage()
            value['aeth_bcc_60s_smoothed_not_normalized'] = bcc_60s_smoothing_not_normalized.getNextAverage()
        
        # Finally print to our tempfiles
        for value in this_runs_cpc_values:
            cpc_writer.writerow([value['cpc_time'],
                                  value['cpc_hour_bin_corrected_timezone'],
                        value['cpc_loc_x'],
                        value['cpc_loc_y'],
                        value['cpc_raw_ppcc'],
                        value['cpc_rectified_ppcc'],
                        value['cpc_corrected_ppcc'],
                        value['instantaneous_backgound_ppcc'],
                        value['hour_average_background_ppcc'],
                        value['cpc_normalized_ppcc']])
            
        for value in this_runs_aeth_values[30:]:
            aeth_writer.writerow([value['aeth_time'],
                                  value['aeth_hour_bin_corrected_timezone'],
                        value['aeth_loc_x'],
                        value['aeth_loc_y'],
                        value['aeth_raw_reference'],
                        value['aeth_raw_sensor'],
                        value['aeth_error'],
                        value['aeth_flow'],
                        value['aeth_bcc_ld30'],
                        value['aeth_p25'],
                        value['aeth_p75'],
                        value['aeth_cev'],
                        value['aeth_so'],
                        value['aeth_bcc_raw'],
                        value['aeth_bcc_corrected'],
                        value['aeth_bcc_rectified'],
                        value['instantaneous_backgound_bcc'],
                        value['hour_average_background_bcc'],
                        value['aeth_normalized_bcc'],
                        value['aeth_bcc_60s_smoothed_normalized'],
                        value['aeth_bcc_60s_smoothed_not_normalized']])




