'''
Created on May 19, 2016

@author: pipes161
'''
from django.forms import ModelForm
from rest.models import pi_uploads

class UploadDataForm(ModelForm):
    class Meta:
        model = pi_uploads
        fields = ['data_file']