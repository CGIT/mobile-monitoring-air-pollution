import os, json, tempfile
import zipfile

from django.core.urlresolvers import reverse
from django.test import TestCase, Client

from rest.models import pi_uploads


# Create your tests here.
class FileUpload(TestCase):
    def setUp(self):
        self.logpath = os.path.join(os.path.dirname(__file__), 'raw_files')
        self.files = os.path.join(os.path.dirname(__file__), 'test_resources/FileUpload')
    
    def test_upload_get(self):
        c = Client()
        response = c.get("/rest/upload", follow=True)
        self.assertEqual(405, response.status_code, "Status code should be 405; GET forbidden. Response code was " + str(response.status_code))
        
    def test_upload_valid_post(self):
        filepath = os.path.join(self.files, 'test_upload_valid_post.zip')
        initial_pi_upload_count = pi_uploads.objects.count()
        c = Client()
        
        with open(filepath, 'rb') as fp:
            data = {
                "data_file" : fp,
                }
            response = c.post(reverse('upload'), data)
            self.assertEqual(response.content, 'success', "Invalid response was: {}".format(response.content))
            self.assertEqual(200, response.status_code, "Status code should be 200. Response code was {}".format(response.status_code))
            
            upload_count_change = pi_uploads.objects.count() - initial_pi_upload_count
            self.assertEqual(upload_count_change, 1, "Upload count did not increment by one. Incremented by {}".format(upload_count_change))
            last_upload = pi_uploads.objects.last()
            self.assertIsNotNone(last_upload.data_file, "File did not seem to upload properly.")
            last_upload_zipfile = zipfile.ZipFile(last_upload.data_file)
            self.assertIsNone(last_upload_zipfile.testzip(), "File is not a valid zip.")    
            
    def test_upload_invalid_extension(self):
        filepath = os.path.join(self.files, 'test_upload_invalid_extension.txt')
        initial_pi_upload_count = pi_uploads.objects.count()        
        c = Client()
        
        with open(filepath, 'rb') as fp:
            data = {
                "data_file" : fp,
                }
            response = c.post(reverse('upload'), data)
            
            content_dict = json.loads(response.content)
            self.assertEqual(content_dict['data_file'][0], u'Invalid extension. Must be .zip.', "File was accepted when it should not have been. Returned {}".format(content_dict['data_file']))
            self.assertEqual(len(content_dict['data_file']), 1, "Incorrect number of errors returned for 'data_file' key. Got {}".format(len(content_dict['data_file'])))
            self.assertEqual(415, response.status_code, "Status code should be 415. Response code was {}".format(response.status_code))  
            
            upload_count_change = pi_uploads.objects.count() - initial_pi_upload_count
            self.assertEqual(upload_count_change, 0, "Upload incremented by {}. Should not increment.".format(upload_count_change))
            
    def test_upload_invalid_contenttype(self):
        filepath = os.path.join(self.files, 'test_upload_invalid_contenttype.zip')
        initial_pi_upload_count = pi_uploads.objects.count()        
        c = Client()
        
        with open(filepath, 'rb') as fp:
            data = {
                "data_file" : fp,
                }
            response = c.post(reverse('upload'), data)
            
            self.assertEqual(415, response.status_code, "Status code should be 415. Response code was {}".format(response.status_code))              
            content_dict = json.loads(response.content)
            self.assertEqual(content_dict['data_file'][0], u'Corrupted zipfile.', "File was accepted when it should not have been. Returned {}".format(content_dict['data_file']))
            self.assertEqual(len(content_dict['data_file']), 1, "Incorrect number of errors returned for 'data_file' key. Got {}".format(len(content_dict['data_file'])))
            
            upload_count_change = pi_uploads.objects.count() - initial_pi_upload_count
            self.assertEqual(upload_count_change, 0, "Upload incremented by {}. Should not increment.".format(upload_count_change))
